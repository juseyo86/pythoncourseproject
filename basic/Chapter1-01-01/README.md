# 기초 프로그램 만들기

### 01. 숫자 맞추기 게임 만들기

![img1](./img/img1.png)
![img2](./img/img2.png)
![img3](./img/img3.png)
![img4](./img/img4.png)


### 02. 컴퓨터의 외부 및 내부 IP 확인하기

![img5](./img/img5.png)
![img6](./img/img6.png)
![img7](./img/img7.png)

### 03. 텍스트를 음성으로 변환하기

![img8](./img/img8.png)
![img9](./img/img9.png)
![img10](./img/img10.png)
![img11](./img/img11.png)
![img12](./img/img12.png)
![img13](./img/img13.png)
