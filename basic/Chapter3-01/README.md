# 기초 프로그램 만들기

## 파이썬을 계산기로 활용하기

![img1](./img/img1.png)
![img2](./img/img2.png)
![img3](./img/img3.png)
![img4](./img/img4.png)
![img5](./img/img5.png)


## 19단 곱셈표 출력하기
![img6](./img/img6.png)
![img7](./img/img7.png)
![img8](./img/img8.png)
![img9](./img/img9.png)
![img10](./img/img10.png)
![img11](./img/img11.png)
![img12](./img/img12.png)
![img13](./img/img13.png)
![img14](./img/img14.png)
![img15](./img/img15.png)
