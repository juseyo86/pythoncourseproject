# 과제 4. 리뷰리스트에서 문장에서 중복제거하고 공통 단어 찾기

### 요구사항

reviewList = ["This is the worst waste of time I have ever experienced. It is a terrible movie.",
"I so much enjoyed this movie I bought a copy to share with my friends. There is something that one of the characters says which may summarize it.",
"When I first saw this movie back, I did not like it. But when I watched this movie a second time, I was deeply moved. It is the best!",
"What is the story and what is it on the screen. I could not even make a laugh while watching this movie."]

- 위 리뷰리스트에서 공통된 단어를 찾는 프로그램을 작성하시오.
- 공통된 단어와 갯수를 같이 표시해야 됩니다.



### 실행결과

[('movie', 5), ('i', 8), ('a', 4), ('the', 5), ('it', 5), ('is', 6), ('this', 5)]

