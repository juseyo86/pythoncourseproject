####  [syllabus 살펴보기](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/syllabus.pdf)

#### 01. [파이썬 살펴보기](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter1-01/README.md)

#### 01-01. [기초 프로그램 만들기1](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter1-01-01/README.md)

#### 01-02. [과제 : 피타고라스의 정의](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter1-01-02/README.md)

#### 02-01. [기초 프로그램 만들기2](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter2-01/README.md)

#### 02-02. [과제 : 윤년 계산 프로그램](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter2-02/README.md)

#### 03-01. [계산기 및 곱셈표](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter3-01/README.md)

#### 03-02. [과제 : 연산자와 두수를 입력받아 함수 계산기 구현](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter3-02/README.md)

#### 04-01. [부가세 출력 프로그램 만들기 및 연습문제](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter4-01/README.md)

#### 04-02. [과제 : 공통 단어 찾기](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter4-02/README.md)

#### 05-01. [파이썬 게임 만들기](https://gitlab.com/juseyo86/pythoncourseproject/-/blob/main/basic/Chapter5-01/README.md)

#### 6주 ~ 7주부터는 개인 교재 구매 후 진행합니다.
- 교재명 : 11가지 프로젝트로 시작하는 Do it! 파이썬 생활 프로그래밍
- 교보문고 링크 : https://product.kyobobook.co.kr/detail/S000001817976
#### * 파이썬 기초프로그램을 마치고 이제부터는 아래와 같은 내용을 시작합니다.
  - 텍스트 파일 가공
  - CSV 파일 다루기
  - 웹 크롤링
  - 데이터 분석
  - 데이터베이스, 웹(html)
  - 시각화



